package com.dreamteam.kitkatjunkies.application.data;

import java.util.List;

/**
 * Created by codetwister on 11/23/13.
 */
public class HomeSensorData {

    private boolean alarm;
    private boolean doorOpen;
    private double ambientTemperature;
    // lights, will change this into dynamic list
    private boolean bedroomLightBulb;
    private boolean kitchenLightBulb;
    private boolean dinerLightBulb;
    private boolean guestroomLightBulb;

    public HomeSensorData() {
    }

    public HomeSensorData(boolean alarm, boolean doorOpen, double ambientTemperature, boolean bedroomLightBulb, boolean kitchenLightBulb, boolean dinerLightBulb, boolean guestroomLightBulb) {
        this.alarm = alarm;
        this.doorOpen = doorOpen;
        this.ambientTemperature = ambientTemperature;
        this.bedroomLightBulb = bedroomLightBulb;
        this.kitchenLightBulb = kitchenLightBulb;
        this.dinerLightBulb = dinerLightBulb;
        this.guestroomLightBulb = guestroomLightBulb;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public boolean isDoorOpen() {
        return doorOpen;
    }

    public double getAmbientTemperature() {
        return ambientTemperature;
    }

    public boolean isBedroomLightBulb() {
        return bedroomLightBulb;
    }

    public boolean isKitchenLightBulb() {
        return kitchenLightBulb;
    }

    public boolean isDinerLightBulb() {
        return dinerLightBulb;
    }

    public boolean isGuestroomLightBulb() {
        return guestroomLightBulb;
    }
}
