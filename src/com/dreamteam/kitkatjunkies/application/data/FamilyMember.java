package com.dreamteam.kitkatjunkies.application.data;

/**
 * Created by codetwister on 11/23/13.
 */
public class FamilyMember {
    private String name;
    private String googleAccount;
    private boolean home;
    private String profilePictureUrl;

    public FamilyMember(String name, String googleAccount, boolean home, String profilePictureUrl) {
        this.name = name;
        this.googleAccount = googleAccount;
        this.home = home;
        this.profilePictureUrl = profilePictureUrl;
    }

    public FamilyMember() {
    }

    public String getName() {
        return name;
    }

    public String getGoogleAccount() {
        return googleAccount;
    }

    public boolean isHome() {
        return home;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }
}
