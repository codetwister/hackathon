package com.dreamteam.kitkatjunkies.application;

import android.app.Application;

import com.dreamteam.kitkatjunkies.application.api.MockApplicationApi;
import com.dreamteam.kitkatjunkies.application.data.FamilyMember;

import java.util.List;

/**
 * Created by codetwister on 11/19/13.
 */
public class KjApplication extends Application {

    private static KjApplication instance;

    private List<FamilyMember> familyMembers;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static KjApplication getInstance() {
        return instance;
    }

    public List<FamilyMember> getFamilyMembers() {
        return familyMembers;
    }

    public void setFamilyMembers(List<FamilyMember> familyMembers) {
        this.familyMembers = familyMembers;
    }
}
