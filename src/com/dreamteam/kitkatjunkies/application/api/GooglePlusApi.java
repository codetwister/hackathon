package com.dreamteam.kitkatjunkies.application.api;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.net.Uri;
import com.dreamteam.kitkatjunkies.application.data.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.people.PersonBuffer;

import java.util.ArrayList;
import java.util.List;

import static com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import static com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import static com.google.android.gms.plus.PlusClient.OnPeopleLoadedListener;

public class GooglePlusApi {

    public static final int REQUEST_CODE_RESOLVE_ERR = 9000;

    public static GooglePlusApi sInstance;

    private Context mContext;
    private PlusClient mPlusClient;
    private ConnectionResult mConnectionResult;

    private GooglePlusApi(Context context, ConnectionCallbacks connectionCallbacks,
                          OnConnectionFailedListener connectionFailedListener) {
        mContext = context;
        mPlusClient = new PlusClient.Builder(context, connectionCallbacks, connectionFailedListener)
                .setActions("http://schemas.google.com/AddActivity")
                .build();
    }

    public static synchronized GooglePlusApi getInstance(Context context, ConnectionCallbacks connectionCallbacks,
                                                         OnConnectionFailedListener connectionFailedListener) {
        if (sInstance == null && context != null && connectionCallbacks != null && connectionFailedListener != null) {
            sInstance = new GooglePlusApi(context, connectionCallbacks, connectionFailedListener);
        }
        return sInstance;
    }

    public void connect() {
        if (mPlusClient != null) {
            mPlusClient.connect();
        }
    }

    public void disconnect() {
        if (mPlusClient != null) {
            mPlusClient.disconnect();
        }
    }

    public boolean isConnected() {
        return mPlusClient != null && mPlusClient.isConnected();
    }

    public boolean isConnecting() {
        return mPlusClient != null && mPlusClient.isConnecting();
    }

    public void signIn() {
        if (mConnectionResult == null) {
            mPlusClient.connect();
        } else {
            try {
                mConnectionResult.startResolutionForResult((Activity) mContext, REQUEST_CODE_RESOLVE_ERR);
            } catch (IntentSender.SendIntentException e) {
                // Try connecting again.
                mConnectionResult = null;
                mPlusClient.connect();
            }
        }
    }

    public void signOut() {
        // Prior to disconnecting, run clearDefaultAccount().
        if (mPlusClient.isConnected()) {
            mPlusClient.clearDefaultAccount();
            mPlusClient.revokeAccessAndDisconnect(new PlusClient.OnAccessRevokedListener() {
                @Override
                public void onAccessRevoked(ConnectionResult connectionResult) {
                    mConnectionResult = null;
                    mPlusClient = null;
                    sInstance = null;
                }
            });
        }
    }

    public User getUserData() {
        final User user = new User();
        user.setAccountName(mPlusClient.getAccountName());
        user.setUserId(mPlusClient.getCurrentPerson().getId());
        user.setDisplayName(mPlusClient.getCurrentPerson().getDisplayName());
        user.setProfilePhotoUrl(mPlusClient.getCurrentPerson().getImage().getUrl());
        return user;
    }

    public void loadPeople(List<String> userIds, OnPeopleLoadedListener onPeopleLoadedListener) {
        if (mPlusClient != null && mPlusClient.isConnected()) {
            mPlusClient.loadPeople(onPeopleLoadedListener, userIds);
        } else {
            onPeopleLoadedListener.onPeopleLoaded(null, null, null);
        }
    }

    private void sharePost(String url, String message) {
        final PlusShare.Builder builder = new PlusShare.Builder((Activity) mContext, mPlusClient);
        // Set call-to-action metadata.
        builder.addCallToAction(
                "CREATE_ITEM", /** call-to-action button label */
                Uri.parse("http://plus.google.com/pages/create"), /** call-to-action url (for desktop use) */
                "/pages/create" /** call to action deep-link ID (for mobile use), 512 characters or fewer */);

        // Set the content url (for desktop use).
        builder.setContentUrl(Uri.parse(url));
        // Set the share text.
        builder.setText(message);
        ((Activity) mContext).startActivityForResult(builder.getIntent(), 0);
    }

    public void setConnectionResult(ConnectionResult connectionResult) {
        mConnectionResult = connectionResult;
    }
}
