package com.dreamteam.kitkatjunkies.application.api;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.dreamteam.kitkatjunkies.application.data.FamilyMember;
import com.dreamteam.kitkatjunkies.application.data.HomeSensorData;

/**
 * Created by codetwister on 11/19/13.
 */
public class MockApplicationApi implements ApplicationApi {

    public HomeSensorData getHomeSensorData() {
        return new HomeSensorData(false, false, 22.5d, true, false, false, true);
    }

}
