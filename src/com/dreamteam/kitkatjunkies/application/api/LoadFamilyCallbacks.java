package com.dreamteam.kitkatjunkies.application.api;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;

import com.dreamteam.kitkatjunkies.application.KjApplication;
import com.dreamteam.kitkatjunkies.application.data.FamilyMember;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.Collection;
import java.util.List;

/**
 * Created by codetwister on 11/24/13.
 */
public class LoadFamilyCallbacks implements LoaderManager.LoaderCallbacks<List<FamilyMember>> {

    private static final String FAMILY_ENDPOINT = "http://gcmcloudserver.appspot.com/family_info.php";

    private Context context;
    private FamilyLoadedCallback familyLoadedCallback;

    public LoadFamilyCallbacks(Context context, FamilyLoadedCallback familyLoadedCallback) {
        this.context = context;
        this.familyLoadedCallback = familyLoadedCallback;
        if (familyLoadedCallback == null) {
            throw new RuntimeException("Please set the callback");
        }
    }

    @Override
    public Loader<List<FamilyMember>> onCreateLoader(int i, Bundle bundle) {
        if (i == 0) {
            return new ListItemLoader(context);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<List<FamilyMember>> objectLoader, List<FamilyMember> o) {
        // set data somewhere and notify listeners;
        KjApplication.getInstance().setFamilyMembers(o);
        familyLoadedCallback.familyLoaded();
    }

    @Override
    public void onLoaderReset(Loader<List<FamilyMember>> objectLoader) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private static class ListItemLoader extends AsyncTaskLoader<List<FamilyMember>> {

        public ListItemLoader(Context context) {
            super(context);
        }

        @Override
        public List<FamilyMember> loadInBackground() {
            try {
                InputStream is = (InputStream) new URL(FAMILY_ENDPOINT).getContent();
                Gson gson = new Gson();
                Type collectionType = new TypeToken<Collection<FamilyMember>>(){}.getType();
                return gson.fromJson(new InputStreamReader(is), collectionType);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    public interface FamilyLoadedCallback {
        public void familyLoaded();
    }
}
