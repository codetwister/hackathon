package com.dreamteam.kitkatjunkies.application.listeners;

/**
 * Created by googlehackaton on 11/23/13.
 */
public interface BluetoothStateListener {
    public void bluetoothConnected();
    public void bluetoothDisconnected();
}
