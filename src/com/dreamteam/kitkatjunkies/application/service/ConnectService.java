package com.dreamteam.kitkatjunkies.application.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.dreamteam.kitkatjunkies.application.listeners.BluetoothStateListener;
import com.dreamteam.kitkatjunkies.util.Constants;

public class ConnectService extends Service implements BluetoothStateListener {

    public static final String TAG = ConnectService.class.getName();

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

	// default name that the Dashboard needs to have in order to automatically
	// connect
	private String dashboardName = "smarthouse";
    private BluetoothService mBluetoothService;
    private BluetoothAdapter mBtAdapter;

	// The BroadcastReceiver that listens for discovered devices
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			// When discovery finds a device
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				// Get the BluetoothDevice object from the Intent
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

				Log.d(TAG, device.getName() + ": " + device.getAddress());

				// if the Dashboard device is discovered, try to connect
				if (device != null && device.getName() != null && device.getName().equals(dashboardName)) {
					connectDevice(device, context, true);
					Log.d(TAG, "Connecting to Dashboard...");
				}
			} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
					.equals(action)) {
				stopSelf();
			}
		}
	};

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public ConnectService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ConnectService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
	public void onCreate() {
		super.onCreate();

        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "ConnectService started");
		return START_STICKY;
	}

    private void connectDevice(BluetoothDevice device, Context context,
                               boolean secure) {
        mBluetoothService = new BluetoothService(context, this);
        // Attempt to connect to the device
        mBluetoothService.connect(device, secure);
    }

    public void startDiscovery() {
        if(mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }
        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        mBtAdapter.startDiscovery();
    }

    public BluetoothService getBluetoothService(){
        return mBluetoothService;
    }

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "Connect Service destroyed");
		unregisterReceiver(mReceiver);
	}

    @Override
    public void bluetoothConnected() {
        sendBroadcast(new Intent(Constants.BLUETOOTH_CONNECTED));
    }

    @Override
    public void bluetoothDisconnected() {
        sendBroadcast(new Intent(Constants.BLUETOOTH_DISCONNECTED));
    }

}
