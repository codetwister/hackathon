package com.dreamteam.kitkatjunkies.util;

import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Parcelable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by codetwister on 11/22/13.
 */
public class RandomUtil {

    private static String lipsum;
    private static String[] words;
    private static int currentWordIndex = 0;

    public static void loadLipsum(Context context) {
        // some mock data to test UI
        try {
            InputStream inputStream = context.getAssets().open("lipsum.txt");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            lipsum = sb.toString();
            words = lipsum.split(" ");
            currentWordIndex = 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String nextWords(int maxChars) {
        StringBuilder sb = new StringBuilder();
        boolean doneBuilding = false;
        while (!doneBuilding) {
            String nextWord = words[currentWordIndex];
            if (nextWord.length() + sb.length() > maxChars) {
                doneBuilding = true;
            } else {
                sb.append(nextWord).append(" ");
                currentWordIndex = (currentWordIndex + 1) % words.length;
            }
        }
        return sb.toString();
    }

    //reads text inside payload
    public static String readPayload(byte[] payload) throws UnsupportedEncodingException {
		 /*
         * payload[0] contains the "Status Byte Encodings" field, per the
         * NFC Forum "Text Record Type Definition" section 3.2.1.
         *
         * bit7 is the Text Encoding Field.
         *
         * if (Bit_7 == 0): The text is encoded in UTF-8 if (Bit_7 == 1):
         * The text is encoded in UTF16
         *
         * Bit_6 is reserved for future use and must be set to zero.
         *
         * Bits 5 to 0 are the length of the IANA language code.
         */

        //Get the Text Encoding
        String textEncoding = ((payload[0] & 0200) == 0) ? "UTF-8" : "UTF-16";

        //Get the Language Code
        int languageCodeLength = payload[0] & 0077;
        String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");

        //Get the Text
        String text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        return text;
    }

    //reads the messages
    public static NdefMessage[] getNdefMessages(Intent intent) {
        // Parse the intent
        NdefMessage[] msgs = null;
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs =
                    intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            } else {
                // Unknown tag type
                byte[] empty = new byte[] {};
                NdefRecord record =
                        new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, empty);
                NdefMessage msg = new NdefMessage(new NdefRecord[] {
                        record
                });
                msgs = new NdefMessage[] {
                        msg
                };
            }
        } else {
            Log.d("NFCRead", "Unknown intent.");
        }
        return msgs;
    }

    public static String getCheckinData(Intent intent) {
        if(intent.getData() != null) {
            return intent.getData().toString();
        }
        return null;
    }
}
