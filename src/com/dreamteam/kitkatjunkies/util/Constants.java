package com.dreamteam.kitkatjunkies.util;

import android.content.Intent;

/**
 * Created by googlehackaton on 11/23/13.
 */
public interface Constants {
    String BLUETOOTH_CONNECTED = "bluetooth_connected";
    String BLUETOOTH_DISCONNECTED = "bluetooth_cdisconnected";
}
