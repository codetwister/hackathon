package com.dreamteam.kitkatjunkies.ui.fragments;

import android.app.LoaderManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dreamteam.kitkatjunkies.R;
import com.dreamteam.kitkatjunkies.application.KjApplication;
import com.dreamteam.kitkatjunkies.application.api.LoadFamilyCallbacks;
import com.dreamteam.kitkatjunkies.application.data.FamilyMember;
import com.dreamteam.kitkatjunkies.ui.views.RemoteImage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by codetwister on 11/23/13.
 */
public class FamilyFragment extends GenericDetailsFragment implements LoadFamilyCallbacks.FamilyLoadedCallback {

    private List<FamilyMember> members = new ArrayList<FamilyMember>();
    private FamilyAdapter adapter;
    private ProgressBar progressBar;

    @Override
    public int getLayoutResourceId() {
        return R.layout.screen_family;
    }

    @Override
    public void setupViews(View rootView) {
        // load family members TODO make this async with loaders
        LoaderManager.LoaderCallbacks<List<FamilyMember>> listItemLoaderCallbacks = new LoadFamilyCallbacks(getActivity(), this);
        getLoaderManager().initLoader(0, null, listItemLoaderCallbacks).forceLoad();

        ListView familyList = (ListView) rootView.findViewById(R.id.family_list);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        adapter = new FamilyAdapter();
        familyList.setAdapter(adapter);
    }

    @Override
    public void familyLoaded() {
        members = KjApplication.getInstance().getFamilyMembers();
        if (members == null) {
            members = new ArrayList<FamilyMember>();
        }
        progressBar.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
    }

    private class FamilyAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return members.size();
        }

        @Override
        public FamilyMember getItem(int i) {
            return members.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = View.inflate(getActivity(), R.layout.list_item_family_member, null);
            }

            // set user name, availability, and profile picture
            View statusBar = view.findViewById(R.id.statusBar);
            RemoteImage profilePic = (RemoteImage) view.findViewById(R.id.profile_pic);
            TextView userName = (TextView) view.findViewById(R.id.user_name);
            TextView userStatus = (TextView) view.findViewById(R.id.user_status);

            FamilyMember member = getItem(i);

            int colorResId = member.isHome() ? R.color.google_green : R.color.google_red;
            statusBar.setBackgroundColor(getResources().getColor(colorResId));
            profilePic.setRemoteImage(member.getProfilePictureUrl());
            userName.setText(member.getName());
            userStatus.setText(String.format("Status %s", member.isHome() ? "home" : "away"));

            return view;
        }
    }

}
