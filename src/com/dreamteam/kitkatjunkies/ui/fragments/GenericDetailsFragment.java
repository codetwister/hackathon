package com.dreamteam.kitkatjunkies.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by codetwister on 11/23/13.
 */
public abstract class GenericDetailsFragment extends Fragment {

    public abstract int getLayoutResourceId();
    public abstract void setupViews(View rootView);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(getLayoutResourceId(), container, false);
        setupViews(v);
        return v;
    }
}
