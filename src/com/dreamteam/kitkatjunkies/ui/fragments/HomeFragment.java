package com.dreamteam.kitkatjunkies.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.dreamteam.kitkatjunkies.R;
import com.dreamteam.kitkatjunkies.application.api.MockApplicationApi;
import com.dreamteam.kitkatjunkies.application.data.HomeSensorData;

import java.text.DecimalFormat;

/**
 * Created by codetwister on 11/23/13.
 */
public class HomeFragment extends GenericDetailsFragment {

    private TextView kitchenBulb;
    private TextView dinerBulb;
    private TextView guestroomBulb;
    private TextView bedroomBulb;
    private TextView alarmState;
    private TextView doorState;
    private TextView ambientTemperature;

    private static final DecimalFormat df = new DecimalFormat("#0.0 C");

    @Override
    public int getLayoutResourceId() {
        return R.layout.screen_home;
    }

    @Override
    public void setupViews(View rootView) {
        kitchenBulb    = (TextView) rootView.findViewById(R.id.kitchen_bulb);
        dinerBulb      = (TextView) rootView.findViewById(R.id.diner_bulb);
        guestroomBulb  = (TextView) rootView.findViewById(R.id.guestroom_bulb);
        bedroomBulb    = (TextView) rootView.findViewById(R.id.bedroom_bulb);
        alarmState = (TextView) rootView.findViewById(R.id.alarm_state);
        doorState      = (TextView) rootView.findViewById(R.id.door_state);
        ambientTemperature = (TextView) rootView.findViewById(R.id.ambient_temperature);

        MockApplicationApi api = new MockApplicationApi();
        HomeSensorData data = api.getHomeSensorData();

        setLightBulb(kitchenBulb, data.isKitchenLightBulb());
        setLightBulb(dinerBulb, data.isDinerLightBulb());
        setLightBulb(guestroomBulb, data.isGuestroomLightBulb());
        setLightBulb(bedroomBulb, data.isBedroomLightBulb());

        alarmState.setText(getString(data.isAlarm() ? R.string.alarm_on : R.string.alarm_off));
        doorState.setText(getString(data.isDoorOpen() ? R.string.door_open : R.string.door_closed));
        ambientTemperature.setText(df.format(data.getAmbientTemperature()));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void setLightBulb(TextView textView, boolean isOn) {
        textView.setCompoundDrawablesWithIntrinsicBounds(null, getActivity().getResources().getDrawable((isOn ? R.drawable.lights_on : R.drawable.lights_off)), null, null);
    }
}
