package com.dreamteam.kitkatjunkies.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.dreamteam.kitkatjunkies.R;
import com.dreamteam.kitkatjunkies.application.api.GooglePlusApi;
import com.dreamteam.kitkatjunkies.ui.views.RemoteImage;

/**
 * Created by codetwister on 11/19/13.
 */
public class ListFragment extends Fragment {

    private NavListener navListener;
    private TextView mTextViewUserName;
    private RemoteImage mRemoteImageAvatar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.navigation_layout, container, false);

        ListView navigationList = (ListView) v.findViewById(R.id.navigation_list);
        final NavigationAdapter adapter = new NavigationAdapter();
        navigationList.setAdapter(adapter);
        navigationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (navListener != null) {
                    NAV_ITEM item = adapter.getItem(i);
                    switch (item) {
                        case FAMILY:
                            navListener.loadDetails(new FamilyFragment());
                            break;
                        case HOME:
                            navListener.loadDetails(new HomeFragment());
                            break;
                        case BOARD:
                            navListener.loadDetails(new BoardFragment());
                            break;
                    }
                }
            }
        });
        mTextViewUserName = (TextView) v.findViewById(R.id.user_name);
        mRemoteImageAvatar = (RemoteImage) v.findViewById(R.id.user_avatar);
        update();
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof NavListener) {
            navListener = (NavListener) activity;
        }
    }

    public void update() {
        GooglePlusApi gPlus = GooglePlusApi.getInstance(getActivity(), null, null);
        String name = Build.MODEL;
        if(gPlus != null && gPlus.isConnected()){
            name = gPlus.getUserData().getDisplayName();
            String imageUrl = gPlus.getUserData().getProfilePhotoUrl();
            mRemoteImageAvatar.setRemoteImage(imageUrl);
        }
        mTextViewUserName.setText(name);
    }

    public interface NavListener {
        public void loadDetails(Fragment fragment);
    }


    private class NavigationAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return NAV_ITEM.values().length;
        }

        @Override
        public NAV_ITEM getItem(int i) {
            return NAV_ITEM.values()[i];
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = View.inflate(getActivity(), R.layout.list_item_navigation, null);
            }

            View navigationColor = view.findViewById(R.id.navigation_color);
            TextView navigationText = (TextView) view.findViewById(R.id.navigation_text);
            ImageView navigationIcon = (ImageView) view.findViewById(R.id.navigation_icon);

            NAV_ITEM navItem = getItem(i);

            navigationColor.setBackgroundColor(navItem.headingColor);
            navigationText.setText(navItem.textResource);
            navigationIcon.setImageResource(navItem.iconResource);

            return view;
        }
    }

    private enum NAV_ITEM {
        FAMILY(R.drawable.family, R.string.nav_family, 0xFF5680fc),
        HOME(R.drawable.home, R.string.nav_home, 0xFF00e13c),
        BOARD(R.drawable.board, R.string.nav_board, 0xFF9e7151);

        private int iconResource;
        private int textResource;
        private int headingColor;

        NAV_ITEM(int iconResource, int textResource, int headingColor) {
            this.iconResource = iconResource;
            this.textResource = textResource;
            this.headingColor = headingColor;
        }
    }
}
