package com.dreamteam.kitkatjunkies.ui.fragments;

import android.view.View;

import com.dreamteam.kitkatjunkies.R;

/**
 * Created by codetwister on 11/23/13.
 */
public class BoardFragment extends GenericDetailsFragment {

    @Override
    public int getLayoutResourceId() {
        return R.layout.screen_board;
    }

    @Override
    public void setupViews(View rootView) {

    }
}
