package com.dreamteam.kitkatjunkies.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by codetwister on 11/23/13.
 */
public class RemoteImage extends ImageView {

    private Handler handler = new Handler(Looper.getMainLooper());

    private DownloadThread downloadThread;

    public RemoteImage(Context context) {
        super(context);
    }

    public RemoteImage(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RemoteImage(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setRemoteImage(String remoteImage) {
        // start download
        downloadThread = new DownloadThread(remoteImage);
        downloadThread.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        // stop download if in progress
        downloadThread.interrupt();
    }

    public void imageLoaded(final Bitmap bmp) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                setImageDrawable(new BitmapDrawable(getResources(), bmp));
            }
        });
    }

    private class DownloadThread extends Thread {
        private String url;

        private DownloadThread(String url) {
            this.url = url;
        }

        @Override
        public void run() {
            try {
                InputStream is = (InputStream) new URL(url).getContent();
                imageLoaded(BitmapFactory.decodeStream(is));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
