package com.dreamteam.kitkatjunkies.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.dreamteam.kitkatjunkies.R;
import com.dreamteam.kitkatjunkies.application.api.GooglePlusApi;
import com.dreamteam.kitkatjunkies.util.ImageUtils;
import com.google.android.gms.common.ConnectionResult;

import static com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import static com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;

public class LoginActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener,
        View.OnClickListener {

    public static final int REQUEST_CODE = 1000;

    private Button mSignInButton;
    private ImageView mLogo;
    private GooglePlusApi mGooglePlusApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        mGooglePlusApi = GooglePlusApi.getInstance(this, this, this);
        setupViews();
    }

    private void setupViews() {
        mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mLogo = (ImageView) findViewById(R.id.logo);

        mSignInButton.setOnClickListener(this);

        Bitmap blurredBitmap = ImageUtils.getBlurredLogo();
        if (blurredBitmap == null) {
            final Bitmap logoBitmap = BitmapFactory.decodeResource(getResources(),
                    R.drawable.kitkat_robot);
            blurredBitmap = ImageUtils.fastBlur(logoBitmap, 35);
            ImageUtils.setBlurredLogo(blurredBitmap);
        }
        mLogo.setImageBitmap(blurredBitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GooglePlusApi.REQUEST_CODE_RESOLVE_ERR) {
            if (resultCode == RESULT_OK) {
                mGooglePlusApi.setConnectionResult(null);
                mGooglePlusApi.connect();
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onDisconnected() {
        // ToDo
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                mGooglePlusApi.signIn();
                break;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mGooglePlusApi.isConnecting()) {
            // The user clicked the sign-in button already. Start to resolve
            // connection errors. Wait until onConnected() to dismiss the
            // connection dialog.
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(this, GooglePlusApi.REQUEST_CODE_RESOLVE_ERR);
                } catch (IntentSender.SendIntentException e) {
                    mGooglePlusApi.connect();
                }
            }
        }
        // Save the result and resolve the connection failure upon a user click.
        mGooglePlusApi.setConnectionResult(connectionResult);
    }
}
