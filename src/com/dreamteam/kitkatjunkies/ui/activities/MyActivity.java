package com.dreamteam.kitkatjunkies.ui.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.dreamteam.kitkatjunkies.R;
import com.dreamteam.kitkatjunkies.application.api.ApplicationApi;
import com.dreamteam.kitkatjunkies.application.service.BluetoothService;
import com.dreamteam.kitkatjunkies.application.api.GooglePlusApi;
import com.dreamteam.kitkatjunkies.application.service.ConnectService;
import com.dreamteam.kitkatjunkies.ui.fragments.BoardFragment;
import com.dreamteam.kitkatjunkies.ui.fragments.FamilyFragment;
import com.dreamteam.kitkatjunkies.ui.fragments.HomeFragment;
import com.dreamteam.kitkatjunkies.ui.fragments.ListFragment;
import com.dreamteam.kitkatjunkies.application.gcm.ServerUtilities;

import com.dreamteam.kitkatjunkies.util.Constants;
import com.dreamteam.kitkatjunkies.util.RandomUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

public class MyActivity extends Activity implements ListFragment.NavListener {

    private static final String TAG = MyActivity.class.getName();

    private static final int REQUEST_ENABLE_BT = 1;

    private DrawerLayout drawerLayout;
    private View navigationFrame;

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    private String SENDER_ID = "370701764936";

    private GoogleCloudMessaging gcm;
    private Context context;
    private String regid;
    private ActionBarDrawerToggle mDrawerToggle;

    private ConnectService mService;
    public boolean mBound;
    private boolean mShouldStartDiscovery;
    private DataUpdateReceiver dataUpdateReceiver = new DataUpdateReceiver();
    private ListFragment mCurrentListFragment;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.LEFT);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.open,  /* "open drawer" description */
                R.string.close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle("");
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                //getActionBar().setTitle("");
            }
        };
        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        navigationFrame = findViewById(R.id.nav_frame);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, new FamilyFragment())
                .replace(R.id.nav_frame, new ListFragment())
                .commit();

        context = getApplicationContext();

        if(checkLoginStatus()) {
            registerWithGCM();
            processIntent(getIntent());
        }

        IntentFilter intentFilter = new IntentFilter(Constants.BLUETOOTH_CONNECTED);
        registerReceiver(dataUpdateReceiver, intentFilter);
    }

    private void registerWithGCM() {
        // Check device for Play Services APK.
        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);

            if (TextUtils.isEmpty(regid)) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
                .getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if(fragment instanceof ListFragment) {
            mCurrentListFragment = (ListFragment) fragment;
        }
    }

    private boolean checkLoginStatus() {
        final GooglePlusApi googlePlusApi = GooglePlusApi.getInstance(this, null, null);
        if (googlePlusApi == null || !googlePlusApi.isConnected()) {
            final Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivityForResult(loginIntent, LoginActivity.REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent serviceIntent = new Intent(this, ConnectService.class);
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dataUpdateReceiver != null) {
            unregisterReceiver(dataUpdateReceiver);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        processIntent(intent);
        // NDEF exchange mode
        // Currently unused because of scheme intent
        /*
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            NdefMessage[] msgs = RandomUtil.getNdefMessages(intent);
            NdefMessage msg=msgs[0];
            NdefRecord record=msg.getRecords()[0];

            String receivedText;
            try {
                receivedText = RandomUtil.readPayload(record.getPayload());
                Toast.makeText(this, "Key token read: " + receivedText, Toast.LENGTH_LONG).show();
                SharedPreferences sharedPreferences = this
                        .getSharedPreferences(getString(R.string.app_name),
                                Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("key", receivedText);
                editor.commit();
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(this, "Invalid format", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
        */
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LoginActivity.REQUEST_CODE:
                if (resultCode == RESULT_OK && GooglePlusApi.getInstance(this, null, null).isConnected()) {
                    Log.d(TAG, "User signed in");
                    registerWithGCM();
                    if(mCurrentListFragment != null) {
                        mCurrentListFragment.update();
                    }
                } else {
                    Log.d(TAG, "Sign in error");
                    finish();
                }
                break;
        }
    }

    private void processIntent(Intent intent) {
        String data = RandomUtil.getCheckinData(getIntent());
        if(!TextUtils.isEmpty(data) && data.contains("checkin")) {
            mShouldStartDiscovery = true;
            Intent serviceIntent = new Intent(this, ConnectService.class);
            startService(serviceIntent);
        }
    }

    public void loadDetails(Fragment details) {
        if (details != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, details)
                    .commit();
            drawerLayout.closeDrawer(navigationFrame);
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (TextUtils.isEmpty(registrationId)) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MyActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new BackgroundRegister().execute();
    }

    private class BackgroundRegister extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String msg = "";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                regid = gcm.register(SENDER_ID);
                msg = "Device registered, registration ID=" + regid;

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                sendRegistrationIdToBackend();

                // For this demo: we don't need to send it because the device
                // will send upstream messages to a server that echo back the
                // message using the 'from' address in the message.

                // Persist the regID - no need to register again.
                storeRegistrationId(context, regid);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String result) {
            //mDisplay.append(result + "\n");
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {
        GooglePlusApi gPlus = GooglePlusApi.getInstance(this, null, null);
        String email = "none";
        String name = Build.MODEL;
        if(gPlus != null && gPlus.isConnected()){
            email = gPlus.getUserData().getAccountName();
            name = gPlus.getUserData().getDisplayName();
        }
        ServerUtilities.register(context, name, email, regid);
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ConnectService.LocalBinder binder = (ConnectService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            if(mShouldStartDiscovery) {
                mShouldStartDiscovery = false;
                mService.startDiscovery();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.BLUETOOTH_CONNECTED)) {
                if (mBound) {
                    GooglePlusApi gPlus = GooglePlusApi.getInstance(MyActivity.this, null, null);
                    String email = "none";
                    String name = Build.MODEL;
                    if (gPlus != null && gPlus.isConnected()) {
                        email = gPlus.getUserData().getAccountName();
                        name = gPlus.getUserData().getDisplayName();
                    }
                    String message = "1," + email + "," + regid + "," + name + ",end";
                    mService.getBluetoothService().write(message.getBytes());
                }
            }
        }
    }

}
